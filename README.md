# Backend

## UML Class Diagram for the Database Relation

The following UML diagram can be view in code in `m̀ain/models.py` as django database models.
![UML](uml/uml.png)


## Install Venv

```
virtualenv venv --python=python3
source venv/bin/activate
pip install -r requirements.txt
```

## Migrate Django Database

```
python manage.py migrate
```


## Start Development Server

Run on localhost on port 8080:

```
python manage.py runserver 8080
```

Run on all available IPs (Dev server shouldn't be used for production):
```
python manage.py runserver 0.0.0.0:8080
```

## Dev
After a change was made to any models.py the following command needs to be run inorder to update the database:
```
python manage.py makemigrations
```

This will create a migration file with which you can update your database with the following command:

```
python manage.py migrate
```
Also the migration file needs to be added to the git repository so that others who are pulling can also migrate!
