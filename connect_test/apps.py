from django.apps import AppConfig


class ConnectTestConfig(AppConfig):
    name = 'connect_test'
