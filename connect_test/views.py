from django.http import HttpResponse

# Create your views here.
def index(request):
    number = request.GET['number']
    return HttpResponse(f"Hello, world. You're at the test index and entered number {number}.")