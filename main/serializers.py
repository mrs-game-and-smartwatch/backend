from django.contrib.auth.models import User
from rest_framework import serializers

from main.models import Profile, Troop, Sport, Match


class UserSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:profile-detail")
    age_range = serializers.SerializerMethodField()
    match_obj_ids = serializers.SerializerMethodField()
    matches = serializers.SerializerMethodField()
    age = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ['url', 'username', 'email', 'bio', 'wants_match', 'score',
                  'verified', 'birth_date', 'gender', 'city', 'location', 'friends', 'troops', 'age_range',
                  'gender_interested', 'distance', 'available_times', 'sport', 'id', 'picture', 'match_obj_ids',
                  'matches', "age"]
        read_only_fields = ('id',)

    def get_age_range(self, obj):
        return (obj.min_age, obj.max_age)

    def get_match_obj_ids(self, obj):
        class SimpleMatch(serializers.ModelSerializer):
            class Meta:
                model = Match
                fields = ["id"]
                read_only_fields = ('id',)

        serializer = SimpleMatch(obj.matches, many=True)
        return serializer.data

    def get_matches(self, obj):
        class SimpleUser(serializers.ModelSerializer):
            class Meta:
                model = User
                fields = ["id", "username"]
                read_only_fields = ('__all__',)

        serializer = SimpleUser(obj.get_matches_users(), many=True)
        return serializer.data

    def get_age(self, obj):
        return obj.age()


class TroopSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:troop-detail")
    score = serializers.SerializerMethodField()
    profiles = serializers.SerializerMethodField()

    class Meta:
        model = Troop
        fields = ['url', 'name', 'score', 'meeting_times', 'sport', 'id', "profiles"]
        read_only_fields = ('id',)

    def get_score(self, obj):
        avg = 0
        num = 0
        for i in obj.profiles.all():
            if i.verified:
                avg += i.score
                num += 1
        if num == 0:
            return 0
        return avg / num

    def get_profiles(self, obj):
        return [{"username": i.username, "id": i.pk} for i in obj.profiles.all()]

class SportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sport
        fields = ['name', 'id']
        read_only_fields = ('id',)


class ProfileSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ["id"]
        read_only_fields = ('id',)


class MatchSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="api:match-detail")

    class Meta:
        model = Match
        fields = ["id", "user_a", "user_b", "url", "user_a_accept", "user_b_accept", "troop_created"]
        read_only_fields = ('id',)
