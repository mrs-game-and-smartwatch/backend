# Create your tests here.
import datetime

from django.contrib.gis.geos import Point
from django.test import TestCase

from .models import Profile, Sport


# Create your tests here.
class TestREST(TestCase):
    def test_create_user(self):
        params = {'username': 'person',
                  'birth_date': str(datetime.date(1990, 10, 7)),
                  'gender': 0b100,
                  'min_age': 21,
                  'max_age': 29,
                  'distance': 1500,
                  'gender_interested': 0b111,
                  'available_times': 0b11111100001100}

        response = self.client.post('/api/profiles/', params)
        self.assertEqual(response.status_code, 201)

    # Test delete maybe?
    def test_fetch_data(self):
        profile = Profile(username="Profile1",
                          birth_date=datetime.date(1990, 10, 15),
                          gender=0b100,
                          min_age=17,
                          max_age=25,
                          location=Point(17.3731, 26.5291),
                          distance=2000,
                          gender_interested=0b011,
                          available_times=0b11001100111111)
        profile.save()

        response = self.client.get(f'/api/profiles/{profile.id}/')
        #response = self.client.get(f'/api/profiles/1/')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['username'], "Profile1")
        self.assertEqual(data['birth_date'], "1990-10-15")
        self.assertEqual(data['gender'], 0b100)
        # self.assertEqual(data['age_range'], '[17, 25]')
        self.assertEqual(data['age_range'], [17, 25])
        self.assertEqual(data['distance'], 2000)
        self.assertEqual(data['gender_interested'], 0b011)

    def test_modify_user(self):
        # Create a user
        profile = Profile(username='Profile',
                          birth_date=datetime.date(1996, 7, 5),
                          gender=0b010,
                          min_age=18,
                          max_age=30,
                          distance=5000,
                          gender_interested=0b100,
                          available_times=0b11001100111111)
        profile.save()

        params = {'username': 'testAccount'}
        response = self.client.patch(
            f'/api/profiles/{profile.id}/', params, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['username'], 'testAccount')
        self.assertEqual(data['birth_date'], '1996-07-05')

    def test_match(self):
        # Create three users
        Sport(name="Basketball").save()
        Sport(name="Football").save()
        profile1 = Profile(username="Profile1",
                           birth_date=datetime.date(1990, 1, 1),
                           gender=0b010,
                           location='52.5322,13.2890',
                           min_age=25,
                           max_age=35,
                           distance=9999999,
                           gender_interested=0b111,
                           available_times=0b11111111111111)
        profile1.save()
        profile1.sport.set(Sport.objects.filter())
        profile1.save()
        profile2 = Profile(username="Profile2",
                           birth_date=datetime.date(1990, 1, 1),
                           gender=0b010,
                           location='48.1955,11.6417',
                           min_age=25,
                           max_age=35,
                           distance=9999999,
                           gender_interested=0b111,
                           available_times=0b11111111111111)
        profile2.save()
        profile2.sport.set(Sport.objects.filter())
        profile2.save()

        # These should match the way they are now
        response = self.client.get(
            f'/api/profiles/{profile1.id}/trigger-matching-detail/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [{"id": profile2.id}])

        # Change profile2's age to be outside of profile1's desire
        params = {'birth_date': datetime.date(1950, 1, 1)}
        response = self.client.patch(
            f'/api/profiles/{profile2.id}/',
            params, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f'/api/profiles/{profile1.id}/trigger-matching-detail/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        # Revert previous changes and change profile2's desired gender to exclude profile1
        params = {'birth_date': datetime.date(1990, 1, 1),
                  'gender_interested': 0b100}
        response = self.client.patch(
            f'/api/profiles/{profile2.id}/',
            params, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f'/api/profiles/{profile1.id}/trigger-matching-detail/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        # Revert previous changes and change profile2's desired distance to exclude profile1
        params = {'gender_interested': 0b111, 'distance': 500}
        response = self.client.patch(
            f'/api/profiles/{profile2.id}/', params, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f'/api/profiles/{profile1.id}/trigger-matching-detail/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        # Revert previous changes and change profile2's available days to avoid matching
        params = {'distance': 9999999, 'available_times': 0}
        response = self.client.patch(
            f'/api/profiles/{profile2.id}/', params, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f'/api/profiles/{profile1.id}/trigger-matching-detail/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        # Revert previous changes and change profile2's sports to avoid matching
        params = {'available_times': 0b11111111111111, 'sport': []}
        response = self.client.patch(
            f'/api/profiles/{profile2.id}/', params, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            f'/api/profiles/{profile1.id}/trigger-matching-detail/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])
