import datetime
import math
from datetime import date

from bitfield import BitField
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from location_field.models.plain import PlainLocationField

time_bitfield_flags = (
    ('mo_0', 'Monday morning'),
    ('mo_1', 'Monday afternoon'),
    ('tu_0', 'Tuesday morning'),
    ('tu_1', 'Tuesday afternoon'),
    ('we_0', 'Wednesday morning'),
    ('we_1', 'Wednesday afternoon'),
    ('th_0', 'Thursday morning'),
    ('th_1', 'Thursday afternoon'),
    ('fr_0', 'Friday morning'),
    ('fr_1', 'Friday afternoon'),
    ('sa_0', 'Saturday morning'),
    ('sa_1', 'Saturday afternoon'),
    ('su_0', 'Sunday morning'),
    ('su_1', 'Sunday afternoon'),
)
gender_flags = (
    'male',
    'female',
    'diverse',
)


class Match(models.Model):
    user = models.ManyToManyField("Profile", related_name="matches")
    user_a = models.ForeignKey("Profile", on_delete=models.CASCADE, related_name="user_a_donttouch")
    user_b = models.ForeignKey("Profile", on_delete=models.CASCADE, related_name="user_b_donttouch")
    user_a_accept = models.CharField(max_length=1, choices=[('0', 'pending'), ('1', 'accepted'), ('2', 'declined')],
                                     default='0', null=False)
    user_b_accept = models.CharField(max_length=1, choices=[('0', 'pending'), ('1', 'accepted'), ('2', 'declined')],
                                     default='0', null=False)
    troop_created = models.BooleanField(default=False, null=False)

    class Meta:
        unique_together = ('user_a', 'user_b',)



    @staticmethod
    def create(a, b, **kwargs):
        m = Match(user_a=a, user_b=b, **kwargs)
        m.save()
        m.user.add(a)
        m.user.add(b)
        return m

    def get_other_user(self, a):
        if self.user_a.pk == a.pk:
            return self.user_b
        else:
            return self.user_a

    def accept(self, pk, accept=True):
        """
        return: Whether both accepted and the troop was created
        """
        if self.user_a.id == pk:
            if not accept:
                self.user_a_accept = "2"
                self.save()
                return None
            self.user_a_accept = "1"
        elif self.user_b.pk == pk:
            self.user_b_accept = "1"
            if not accept:
                self.user_b_accept = "2"
                self.save()
                return None
        else:
            raise RuntimeError(f"Primary key {pk} does not exists for this matching")
        if self.user_a_accept == "1" and self.user_b_accept == "1":
            # create new troop
            t = Troop(name=f"{self.user_a.username} and {self.user_b.username}'s troop",
                      score=0,
                      meeting_times=self.user_a.available_times & self.user_b.available_times)
            t.save()
            t.sport.set(self.user_a.sport.all().intersection(self.user_b.sport.all()))
            self.user_a.troops.add(t)
            self.user_b.troops.add(t)
            self.troop_created = True
            self.save()
            return t
        self.save()
        return None

    def __str__(self):
        return f"{self.user_a.username} and {self.user_b.username}'s match"


class Profile(models.Model):
    username_validator = UnicodeUsernameValidator()
    email = models.EmailField(('email address'), blank=True)
    username = models.CharField(
        ('username'),
        max_length=150,
        unique=True,
        help_text=('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': ("A user with that username already exists."),
        },
    )
    first_name = models.CharField(('first name'), max_length=30, blank=True)
    last_name = models.CharField(('last name'), max_length=150, blank=True)
    picture = models.ImageField(upload_to='profile_pics/', default='profile_pics/default.jpg', null=False)
    bio = models.CharField(max_length=256, null=True, default="Hi, I just joined TrainingTroop!", blank=True)
    wants_match = models.BooleanField(default=False, null=False)
    score = models.IntegerField(default=0, null=False)
    verified = models.BooleanField(default=False, null=False)
    birth_date = models.DateField(null=False, blank=False, default=datetime.date(1997, 9, 20))
    gender = BitField(flags=gender_flags, default=0, null=True)
    city = models.CharField(max_length=255, default="Munich")
    location = PlainLocationField(based_fields=['city'], zoom=7, null=True, blank=True)
    # matches = models.ManyToManyField("self", blank=True)
    friends = models.ManyToManyField("self", blank=True)

    troops = models.ManyToManyField("Troop", related_name="profiles", blank=True)

    min_age = models.IntegerField(default=0, null=False)
    max_age = models.IntegerField(default=100, null=False)
    gender_interested = BitField(flags=gender_flags, default=0)
    distance = models.IntegerField(default=50000, null=False)
    available_times = BitField(flags=time_bitfield_flags, default=0)
    sport = models.ManyToManyField("Sport", blank=True, related_name="profiles")

    def get_matches_users(self):
        return set([i.get_other_user(self) for i in self.matches.all()])

    def __str__(self):
        return self.username

    def get_lat_long(self):
        corr = self.location.split(",")
        return (float(corr[0]), float(corr[1])) if self.location != "" else (0, 0)

    def age(self):
        return math.floor((date.today() - self.birth_date).days / 365.2425)


class Sport(models.Model):
    name = models.CharField(max_length=64, null=False, default="Some Sport")

    def __str__(self):
        return self.name


class Troop(models.Model):
    name = models.CharField(max_length=64, default="New Group", blank=False, null=False)
    score = models.IntegerField(default=0, null=False)
    meeting_times = BitField(flags=time_bitfield_flags, default=0)
    sport = models.ManyToManyField("Sport", blank=True, related_name="troops")

    def generate_score(self):
        avg = 0
        for i in self.profiles.objects():
            avg += i.score
        self.score = avg / len(self.profiles.objects)
        self.save()

    def __str__(self):
        return self.name
