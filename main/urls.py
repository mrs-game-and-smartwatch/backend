from django.urls import include, path
from rest_framework import routers

from main import views

app_name = 'main'

router = routers.DefaultRouter()
router.register(r'profiles', views.UserViewSet)
router.register(r'troops', views.TroopViewSet)
router.register(r'sports', views.SportViewSet)
router.register(r'matches', views.MatchViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
