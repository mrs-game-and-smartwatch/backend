from bitfield import BitField
from bitfield.forms import BitFieldCheckboxSelectMultiple
from django.contrib import admin

from .models import Profile, Sport, Troop, Match


# Register your models here.


class ProfileAdmin(admin.ModelAdmin):
    formfield_overrides = {
        BitField: {'widget': BitFieldCheckboxSelectMultiple},
    }


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Sport)
admin.site.register(Troop, ProfileAdmin)
admin.site.register(Match)
