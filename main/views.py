import itertools
import json
import os
import random
from math import sin, cos, sqrt, atan2, radians
from pathlib import Path

from django.db.models import Q
from django.shortcuts import get_object_or_404
# Create your views here.
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.models import Profile, Troop, Sport, Match
from main.serializers import UserSerializer, TroopSerializer, SportSerializer, ProfileSerializerSimple, MatchSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Profile.objects.all()
    serializer_class = UserSerializer

    @staticmethod
    def common_sports(user_a: Profile, user_b: Profile):
        return user_a.sport.all().intersection(user_b.sport.all()).exists()

    @staticmethod
    def geo_dist(coord_a, coord_b):
        # approximate radius of earth in m
        R = 6373000.0

        lat1 = radians(coord_a[0])
        lon1 = radians(coord_a[1])
        lat2 = radians(coord_b[0])
        lon2 = radians(coord_b[1])

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        return R * c

    @staticmethod
    def is_match(user_a: Profile, user_b: Profile):
        # todo can be made more efficient if not all cases are checked in case of mismatch
        age_a = user_a.age()
        age_b = user_b.age()
        # age match
        a = user_a.min_age <= age_b <= user_a.max_age and user_b.min_age <= age_a <= user_b.max_age
        # gender match
        b = user_a.gender_interested & user_b.gender and user_b.gender_interested & user_a.gender
        # check distance
        dist = UserViewSet.geo_dist(user_a.get_lat_long(), user_b.get_lat_long())
        c = dist <= min(user_a.distance, user_b.distance)
        # check common times
        d = user_a.available_times & user_b.available_times
        # common sports
        e = UserViewSet.common_sports(user_a, user_b)
        return a and b and c and d and e

    @action(methods=['get'], url_path='trigger-matching', detail=False)
    def trigger_matching(self, request):
        for i, user_a in enumerate(Profile.objects.all()):
            for j, user_b in enumerate(Profile.objects.all()):
                if user_a.pk == user_b.pk \
                        or user_b in user_a.get_matches_users():
                    continue
                if j > i:
                    # to aviod unnecessary looping because symmetric
                    break
                elif UserViewSet.is_match(user_a, user_b):
                    # yay we found a match that does not exist--> save the match
                    # Match(user_a=user_a, user_b=user_b).save()
                    Match.create(user_a, user_b)
        return Response({"success": True}, status=status.HTTP_200_OK)

    @action(methods=['post'], url_path='accept-match', detail=True)
    def accept_match(self, request, pk=None):
        j = json.loads(request.body)
        accept = j.get("accept", "1")
        pk_b = j.get("id", -1)
        user_a = get_object_or_404(Profile, id=pk)
        user_b = get_object_or_404(Profile, id=pk_b)
        m = get_object_or_404(user_a.matches, Q(user_a_id=user_b.pk) | Q(user_b_id=user_b.pk))
        troop = m.accept(int(pk), bool(int(accept)))
        if troop:
            return Response({"success": True, "name": troop.name, "id": troop.id}, status=status.HTTP_200_OK)
        else:
            return Response({"success": False}, status=status.HTTP_200_OK)

    @action(methods=['get'], url_path='trigger-matching-detail', detail=True)
    def trigger_matching_detail(self, request, pk=None):
        user_a = get_object_or_404(Profile, id=pk)
        for j, user_b in enumerate(Profile.objects.all()):
            e = user_a.matches.filter(Q(user_a_id=user_b.pk) | Q(user_b_id=user_b.pk)).exists()
            m = UserViewSet.is_match(user_a, user_b)
            if user_a.pk == user_b.pk:
                continue
            elif m and not e:
                # yay we found a match that does not exists --> save the match
                Match.create(user_a, user_b)
            elif not m and e:
                # match exists but no match calculated --> delete maybe?
                match = user_a.matches.filter(Q(user_a_id=user_b.pk) | Q(user_b_id=user_b.pk)).first()

                if match.user_a_accept == "0" and match.user_a_accept == "0":
                    # only delete when both users didnt do anything so far
                    user_a.matches.filter(Q(user_a_id=user_b.pk) | Q(user_b_id=user_b.pk)).first().delete()

        serializer = ProfileSerializerSimple(user_a.get_matches_users(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], url_path='possbile-groups-until-n', detail=True)
    def possbile_groups_until_n(self, request, pk=None):
        n = request.GET.get("n", 5)
        user_a = get_object_or_404(Profile, id=pk)
        matches = user_a.get_matches_users()
        possible_matches = []
        for i in range(2, n):
            # i+1 is the number of useres in the group
            cross = [matches] * i
            cross_tuple_list = itertools.product(*cross)
            cross_set_list = [set(j) for j in cross_tuple_list if len(set(j)) == i]
            cross_set_set = set().union(*cross_set_list)

            for element in cross_set_set:
                # element = (user_a, ) + element
                # time match
                a = user_a.available_times
                for j in element:
                    a &= j.available_times
                # common sports
                b = user_a.sport
                for j in element:
                    b = b.intersection(j.sport)
                b = b.exists()
                # check age
                # min_age = user_a.min_age
                # max_age = user_a.max_age
                # for j in element:
                #     min_age = max(j.min, min_age)
                #     max_age = min(j.max, max_age)
                if a and b:
                    possible_matches.append(element)
        serializer = ProfileSerializerSimple(possible_matches, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], url_path='upload-pic', detail=True)
    def upload_pic(self, request, pk=None):
        user_a = get_object_or_404(Profile, id=pk)
        file = request.data['file']
        _, file_extension = os.path.splitext(file.name)
        user_a.picture.delete()
        s = f"{str(random.random())[2:12]}{file_extension.lower()}"
        while (Path("media/profile_pics") / s).exists():
            s = f"{str(random.random())[2:12]}{file_extension.lower()}"
        file.name = s
        user_a.picture = file
        user_a.save()
        return Response({"filename": file.name}, status=status.HTTP_200_OK)

    @action(methods=['get'], url_path='troops', detail=True)
    def troops(self, request, pk=None):
        user_a = get_object_or_404(Profile, id=pk)
        serializer = TroopSerializer(user_a.troops, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], url_path='login', detail=False)
    def login(self, request):
        user_a = get_object_or_404(Profile, username=request.GET.get("username", ""))
        # del request.GET["username"]
        serializer = UserSerializer(user_a, many=False, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class TroopViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Troop.objects.all()
    serializer_class = TroopSerializer


class SportViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Sport.objects.all()
    serializer_class = SportSerializer


class MatchViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
